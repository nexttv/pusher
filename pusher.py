#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json, os, sys, signal

import gevent_inotifyx as inotify
import pymongo
import rados
import gevent
from gevent import monkey

import argparse
import logging

# TODO
# order of segments by index
# bulk insert
# cassandra

CEPH_CONF_FILE = "/etc/ceph/ceph.conf"
CEPH_KEYRING = "/etc/ceph/ceph.client.admin.keyring"
# CEPH_KEYRING = "/etc/ceph/ceph.mon.keyring"
SEGMENTS_PATH = "/segs/"

class DataSender(object):
    def __init__(self, config):
        try:
            logging.info("connecting to mongo {} ...".format(config['mongo']))
            self.mongo = pymongo.MongoClient(
                host=config['mongo'],
                max_pool_size=10)
            self.mongo.db.segments.ensure_index([('time', pymongo.ASCENDING)])
            self.mongo.db.segments.ensure_index([('index', pymongo.ASCENDING)])
            self.mongo.db.slices.ensure_index([('start_time', pymongo.ASCENDING)])

        except pymongo.errors.ConnectionFailure as e:
            sys.exit('Connection to mongo failed: %s' % str(e))

        logging.info("... connected to mongo")

        try:
            logging.info('connecting to rados ...')
            self.ceph = rados.Rados(conffile=CEPH_CONF_FILE, conf=dict(keyring=CEPH_KEYRING))
            self.ceph.connect()
            logging.info('... connected to rados')

            logging.info("Cluster ID: %s" % self.ceph.get_fsid())
            logging.info("Ceph stats: %s" % str(self.ceph.get_cluster_stats()))
            if not self.ceph.pool_exists('segments'):
                self.ceph.create_pool('segments')
            self.rados_ioctx = self.ceph.open_ioctx('segments')
            # for o in self.rados_ioctx.list_objects():
            #     logging.info(o.stat(), o.read()
        except Exception as e:
            sys.exit('Connection to ceph failed: %s' % str(e))

    def _send_segment_meta(self, data): # mongo
        logging.info('sending segment meta: segment_id: {} resource_id: {} slice_id: {} type: {} track_id: {} quality_id: {} time: {} index: {}'.format(data['segment_id'], data['resource_id'], data['slice_id'], data['type'], data['track_id'], data['quality_id'], data.get('time', 'missing'), data.get('index', 'missing')))
        self.mongo.db.segments.save(data)

    def _send_slice_meta(self, data):
        self.mongo.db.slices.save(data)

    def _send_file(self, segment_filepath, segment_name): # rados
        try:
            self.rados_ioctx.require_ioctx_open()
            with open(segment_filepath, 'r') as f:
                data = f.read()
            self.rados_ioctx.write_full(str(segment_name), data)
        except rados.IoctxStateError as e:
            raise Exception("Rados connection closed :( : %s" % str(e))

    def send_segments(self, path, segments_ids):
        try:

            logging.info("sending segments {}".format(segments_ids))

            for segment_id in segments_ids:
                segment_filepath = os.path.join(path, '%s.data' % segment_id)
                self._send_file(segment_filepath, segment_id)
                os.remove(segment_filepath)

            for segment_id in segments_ids:
                meta_filepath = os.path.join(path, '%s.meta' % segment_id)
                with open(meta_filepath, 'r') as f:
                    data = json.load(f)
                if data['type'] == 'data':
                    data['time'] = long(data['time'])
                    data['index'] = long(data['index'])
                    data['duration'] = long(data['duration'])
                self._send_segment_meta(data)
                os.remove(meta_filepath)

        except pymongo.errors.OperationFailure as e:
            logging.error("One of this segments %s meta not saved: %s" % (str(segments_ids), str(e)))
        except rados.LogicError:
            logging.error("One of this segments %s not saved: %s" % (str(segments_ids), str(e)))
        except Exception as e:
            logging.error("Some error on send_segments(%s): %s" % (str(segments_ids), str(e)))

    def send_slice(self, path, slice_file):
        try:
            filepath = os.path.join(path, slice_file)
            with open(filepath, 'r') as f:
                data = json.load(f)

            logging.info("sending slice slice_id: {} resource_id: {} start_time: {}".format(data.get('slice_id', 'missing'), data.get('resource_id', 'missing'), data.get('start_time', 'missing')))
            data['start_time'] = long(data['start_time'])
            self._send_slice_meta(data)
            os.remove(filepath)
        except pymongo.errors.OperationFailure as e:
            logging.error("Slice meta data not saved for file '%s': %s" % (slice_file, str(e)))
        except Exception as e:
            logging.error("Some error on send_slice(%s): %s" % (slice_file, str(e)))

class Pusher(object):
    def __init__(self, config):
        self.config = config
        self.path = SEGMENTS_PATH
        self.sender = DataSender(self.config)
        self.file_watcher = None

    def start(self):
        try:
            if os.path.exists(self.path):
                logging.info('found segment directory: scan for existing segments ...')
                file_list = os.listdir(self.path)
                for filename in file_list:
                    self.test_file(filename)
                logging.info('... scanning done')
            else:
                sys.exit('did not found segment dir: did you forgot to mount segment dir volume to /segs/ ?')
            self.start_watch()
        except Exception as e:
            logging.error("Some error on Pusher.start: %s" %  str(e))

        logging.info('started')

    def start_watch(self):
        logging.info('starting watcher')
        self.file_watcher = gevent.spawn(self._file_notifier)

    def stop_watch(self):
        if not self.file_watcher is None:
            self.file_watcher.kill()
            self.file_watcher = None

    def test_file(self, filename):

        logging.info('testing file {}'.format(filename))

        try:
            if filename.endswith(".push"):
                filepath = os.path.join(self.path, filename)
                with open(filepath, 'r') as f:
                    segments = [x.strip() for x in f]
                self.sender.send_segments(self.path, segments)
                os.remove(filepath)
            elif filename.endswith(".slice"):
                self.sender.send_slice(self.path, filename)
        except Exception as e:
            logging.error("Some error on test_file: %s" %  str(e))

    def _file_notifier(self):
        logging.info('file_notifier: starting inotify...')
        fd = inotify.init()
        logging.info('... starting inotify done')
        logging.info('started watcher for {}'.format(self.path))

        try:
            inotify.add_watch(fd, self.path, inotify.IN_MOVED_TO | inotify.IN_CLOSE_WRITE)
            while True:
                events = inotify.get_events(fd)
                for event in events:
                    self.test_file(event.name)
                gevent.sleep(0)
        except Exception as e:
            logging.error("Some error on _file_notifier: %s" %  str(e))
        finally:
            os.close(fd)

def parse_cmd():

    parser = argparse.ArgumentParser()
    # parser.add_argument('--mongo', dest='mongo', required=False, help='mongo db connection uri')
    opts = parser.parse_args()

    return opts

def parse_env():

    opts = {}

    m = os.environ.get('MONGO_CONN_STR')
    if m != None:
        opts['mongo'] = m

    return opts

def read_conf():

    conf = {
        "mongo" : "mongodb://127.0.0.1:27017"
    }

    opts = parse_env()
    conf.update(opts)

    opts = parse_cmd()
    opts = vars(opts)
    conf.update(opts)

    logging.info('configuration: {}'.format(conf))
    return conf

def main():

    monkey.patch_all()

    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    config = read_conf()

    pusher = Pusher(config)

    def end_now():
        pusher.stop_watch()
        sys.exit()

    gevent.signal(signal.SIGINT, end_now)
    gevent.signal(signal.SIGTERM, end_now)

    pusher.start()
    gevent.wait()

main()

